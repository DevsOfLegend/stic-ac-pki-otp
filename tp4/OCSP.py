#!/usr/bin/python
# coding= utf8
import sys, os;
import subprocess;
from tempfile import mkstemp;

__BOUNDARY_B__ = '-----BEGIN CERTIFICATE-----';
__BOUNDARY_E__ = '-----END CERTIFICATE-----';
__CERTIFICAT_SERVEUR__ = 'certificat_serveur.pem';
__CERTIFICAT_CA_INTERMEDIAIRE__ = 'certificat_ca_intermediaire.pem';
__CERTIFICAT_CA__ = 'certificat_ca.pem';


def main( argv ):
	global __CERTIFICAT_SERVEUR__,__CERTIFICAT_CA_INTERMEDIAIRE__,__CERTIFICAT_CA__;
	if len(argv) <= 1:
		print 'Usage:';
		print '	'+argv[0]+' google.fr';
		return 0;

	server = argv[1]+':443';

	noReDownload = len(argv) == 3 and argv[2] == 'bypass';

	__CERTIFICAT_SERVEUR__ = argv[1]+'.'+__CERTIFICAT_SERVEUR__;
	__CERTIFICAT_CA_INTERMEDIAIRE__ = argv[1]+'.'+__CERTIFICAT_CA_INTERMEDIAIRE__;
	__CERTIFICAT_CA__ = argv[1]+'.'+__CERTIFICAT_CA__;

	print '~ Obtention du certificat';
	if not noReDownload or not os.path.isfile(__CERTIFICAT_SERVEUR__) or not os.path.isfile(__CERTIFICAT_CA_INTERMEDIAIRE__) or not os.path.isfile(__CERTIFICAT_CA__):
		p = subprocess.Popen(['openssl', 's_client', '-showcerts', '-connect', server], shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
		if int(p.wait()) != 0:
			print 'Openssl(1) error:';
			print p.stdout.read();
			return 1;
		cert = p.stdout.read();

		############################################################################
		# Certificat_serveur
		##
		posB = cert.find(__BOUNDARY_B__);
		posE = cert.find(__BOUNDARY_E__)+len(__BOUNDARY_E__);

		with open(__CERTIFICAT_SERVEUR__, 'w') as fp:
			fp.write(cert[posB:posE].strip('\r\n\t '));

		############################################################################
		# Certificat_ca_intermediaire
		##
		posB = cert.find(__BOUNDARY_B__, posE);
		posE = cert.find(__BOUNDARY_E__, posE)+len(__BOUNDARY_E__);
		print posB, posE;

		with open(__CERTIFICAT_CA_INTERMEDIAIRE__, 'w') as fp:
			fp.write(cert[posB:posE].strip('\r\n\t '));

		############################################################################
		# Certificat_CA
		##
		posB = cert.find(__BOUNDARY_B__, posE);
		posE = cert.find(__BOUNDARY_E__, posE)+len(__BOUNDARY_E__);
		print posB, posE;

		with open(__CERTIFICAT_CA__, 'w') as fp:
			fp.write(cert[posB:posE].strip('\r\n\t '));


	############################################################################
	# Récupération de l'url d'accès au serveur OCSP
	##
	print u'~ Récupèration de l\'URL d\'accès au serveur OCSP';
	p = subprocess.Popen(['openssl', 'x509', '-noout', '-ocsp_uri', '-in', __CERTIFICAT_SERVEUR__], shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
	if int(p.wait()) != 0:
		print 'Openssl(1) error:';
		print p.stdout.read();
		return 1;
	url_OCSP = p.stdout.read().strip('\r\n\t ');


	############################################################################
	# Récupération du serial
	##
	print u'~ Récupèration du serial';
	p = subprocess.Popen(['openssl', 'x509', '-noout', '-serial', '-in', __CERTIFICAT_SERVEUR__], shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
	if int(p.wait()) != 0:
		print 'Openssl(1) error:';
		print p.stdout.read();
		return 1;
	serial_OCSP = p.stdout.read().strip('\r\n\t ');
	serial_OCSP = '0x'+serial_OCSP[serial_OCSP.find('=')+1:];


	############################################################################
	# Récupération du serial
	##
	print u'~ Vérification de la validité du certificat serveur';
	p = subprocess.Popen(['openssl', 'ocsp', '-issuer', __CERTIFICAT_SERVEUR__, '-url', url_OCSP, '-serial', serial_OCSP, '-text', '-noverify'], shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
	if int(p.wait()) != 0:
		print 'Openssl(1) error:';
		print p.stdout.read();
		return 1;

	print p.stdout.read().strip('\r\n\t ');

	return 0;


if __name__ == '__main__':
	exit(main(sys.argv));
