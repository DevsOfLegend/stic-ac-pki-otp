#!/usr/bin/python
# coding= utf8
import sys, os;
import subprocess, httplib;
from tempfile import mkstemp;


################################################################################
# @brief Main
# @param[in] argv	{List<Str>} Argv
# @return {int} 0 Si tout s'est bien passé.
##
def main(argv):
	if len(argv) <= 1:
		help();
		return 0;

	isChecker = False;
	fileToHorodate = argv[1];
	if len(argv) == 3:
		isChecker = argv[1] == 'check';
		fileToHorodate = argv[2];

	if not os.path.isfile(fileToHorodate):
		print 'File not found: <'+fileToHorodate+'>';
		return 1;

	tmp,tmp_query = mkstemp();

	if isChecker:
		checkFile( fileToHorodate );
	else:
		genTSR(tmp_query, fileToHorodate);
		checkFile( fileToHorodate );


	os.remove(tmp_query);


################################################################################
# @brief Permet de générer un tsr pour un fichier {fileToHorodate}
# @param[in] tmp_query			{Str} Adresse vers un fichier temporaire
# @param[in] fileToHorodate		{Str} Le fichier a horodater
# @return[None]
##
def genTSR( tmp_query, fileToHorodate ):
	print '~ Generate SHA1';
	p = subprocess.Popen(['openssl', 'ts', '-query', '-data', fileToHorodate, '-sha1', '-out', tmp_query], shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
	if int(p.wait()) != 0:
		print 'Openssl(1) error:';
		print p.stdout.read();
		print p.stderr.read();
		return;


	with open(tmp_query, 'rb') as fp:
		print '~ Send request to timestamping.edelweb.fr';
		req = httplib.HTTPConnection('timestamping.edelweb.fr');
		req.request('POST', '/service/tsp', fp, {'Content-Type': 'application/timestamp-request'});
		queryReply = req.getresponse();
		if queryReply.status != 200:
			print 'HTTP error ('+str(queryReply.status)+'):';
			print queryReply.read();

	print '~ Write tsr';
	with open(fileToHorodate+'.tsr', 'wb') as fp:
		fp.write(queryReply.read());

	print '~ Read tsr';
	p = subprocess.Popen(['openssl', 'ts', '-reply', '-in', fileToHorodate+'.tsr', '-text'], shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
	if int(p.wait()) != 0:
		print 'Openssl(2) error:';
		print p.stdout.read();
		return;

	#p.stdout.read().strip(' \t\r\n');


################################################################################
# @brief Permet de vérifier l'horodatage d'un fichier
# @param[in] fileToHorodate		{Str} Le fichier a tester
# @return[None]
##
def checkFile( fileToHorodate ):
	if not os.path.isfile(fileToHorodate+'.tsr'):
		print 'No TSR for <'+fileToHorodate+'>';
		return ;

	if not os.path.isfile('Clepsydre.cacert.cer'):
		print '~ Download Clepsydre.cacert.cer';
		req = httplib.HTTPConnection('timestamping.edelweb.fr');
		req.request('GET', '/Clepsydre.cacert');
		queryReply = req.getresponse();
		if queryReply.status != 200:
			print 'HTTP error ('+str(queryReply.status)+'):';
			print queryReply.read();

		print '~ Write Clepsydre.cacert.cer';
		with open('Clepsydre.cacert.cer', 'wb') as fp:
			fp.write(queryReply.read());


	p = subprocess.Popen(['openssl', 'ts', '-verify', '-data', fileToHorodate, '-in', fileToHorodate+'.tsr', '-CAfile', 'Clepsydre.cacert.cer'], shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
	if int(p.wait()) != 0:
		print 'Openssl(2) error:';
		print p.stdout.read().strip('\r\n\t ');
		return 1;
	#print p.stdout.read().strip('\r\n\t ');
	ret = p.stdout.read().strip(' \t\r\n');
	return ret.endswith('Verification: OK');


################################################################################
# @brief Permet d'afficher l'aide
# @return[None]
##
def help():
	print 'Usage:';
	print '	'+sys.argv[0]+' [check] fileToHorodate';


if __name__ == '__main__':
	exit(main(sys.argv));
