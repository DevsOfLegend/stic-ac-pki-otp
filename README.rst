======
README
======

:Info: See <https://bitbucket.org/DevsOfLegend/stic-ac-pki-otp> for repo.
:Author: NUEL Guillaume (ImmortalPC) and DECK Léa (dla)
:Date: $Date: 2013-05-20 $
:Revision: $Revision: 10 $
:Description: TP et Projet pour le cours de Master 1 de Cryptis: Sécurité des TIC. Le projet porte sur la création d'une AC et de la création d'un site web d'horodatage sécurisé avec SSL. (Tout les certificats sont gérés avec l'AC). La connexion utilisateur se faire grâce à l'algorithme OTP de Google.


**Description**
---------------
TP et Projet pour le cours de Master 1 de Cryptis: Sécurité des TIC. Le projet porte sur la création d'une AC et de la création d'un site web d'horodatage sécurisé avec SSL. (Tout les certificats sont gérés avec l'AC). La connexion utilisateur se faire grâce à l'algorithme OTP de Google.

|
| Ce programme comprend les fonctionnalité suivantes:

- Gestion d'une AC.
- Création d'un certificat pour une connexion SSL
- Création d'un certificat pour l'horodatage
- Création de l'algorithme OTP selon l'algorithme de Google
- Système de connexion utilisateur avec OTP.
- Génération de l'OTP à la volé en Javascript
- Sécurisation du serveur via un tunnel socat en SSL
- Système d'horodatage en ligne:
    - Obtention d'une preuve de dépôt à une date donnée
    - Vérification de la preuve de dépôt.
- Interface web en html5, css3, Javascript


**Format** **du** **code**
--------------------------
- Le code est en UTF-8
- Le code est indenté avec des tabulations réel (\\t)
- Le code est prévus pour être affiché avec une taille de 4 pour les tab (\\t)
- Les fins de lignes sont de type LF (\\n)
- IDE utilisé: Geany


**Outils nécessaires**
----------------------
- Python 2
- socat (tunnel SSL)
- OpenSSL > 1.0
- Navigateur web :p


**Normes**
----------
- Les commentaires sont au format doxygen.


**Utilisation**
---------------
Le projet se compose en 3 parties:

- Partie génération des certificats
- Partie local avec génération de l'OTP en ligne de commande
- Partie serveur avec le système d'horodatage + OTP en JavaScript

Note: À partir de cette ligne toutes les commandes indiquées seront effectuées depuis le dossier **projet**.

Pour générer les certificats: AC racine, SSL, Horodatage:

>>> cd MY_CA && ./makeCertif.sh

Pour générer un OTP en ligne de commande

>>> ./OTP.PY --help

Pour lancer le serveur python: (Le serveur se lance sur http://127.0.0.1:8000/ )

>>> ./basic_pyServ.sh

Pour lancer le tunnel SSL: (Le tunnel se lance sur http://127.0.0.1:4443/ )

>>> ./basic_ssl.sh

.. Attention::
	Pour avoir un mode console correct, veuillez utiliser un système **UNIX**.
	La console a un mode d'affichage qui est inconfortable a lire (voir illisible) sous Windows.


**Documentation**
-----------------
| Pour plus d'information voir la documentation dans le dossier **/projet/doc/**
| Énoncer du projet: **/projet/doc/PROJET.pdf**
| Rpport du projet; **/projet/doc/rapport.pdf**


**IDE** **RST**
---------------
RST créé grâce au visualisateur: https://github.com/anru/rsted
