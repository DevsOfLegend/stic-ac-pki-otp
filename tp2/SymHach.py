#!/usr/bin/python
# coding= utf8

import sys, os, subprocess;
from operator import xor;

__PASSWORD__ = 'hello the world';
__FILL__ = '=';

def main( argv ):
	if len(argv) <= 2:
		print 'Usage: '+argv[0]+' myFile hachSize';
		return 0;

	try:
		myFile = sys.argv[1];
		if not os.path.isfile(myFile):
			raise Exception(myFile+' not found');
		hachSize = int(sys.argv[2]);
	except Exception as m:
		print m;
		return 0;

	returned = subprocess.Popen(['openssl', 'enc', '-des-cbc', '-nosalt', '-k', __PASSWORD__, '-in', myFile], stdout=subprocess.PIPE, stderr=subprocess.PIPE);
	finalData = returned.stdout.read(hachSize);

	current = '';
	while True:
		current = returned.stdout.read(hachSize);
		if len(current) == 0:
			break;

		while len(current) < hachSize:
			current += __FILL__;

		finalData = xor_strings(finalData, current);

	print finalData.encode("hex");


def xor_strings(xs, ys):
	return "".join(chr(ord(x) ^ ord(y)) for x, y in zip(xs, ys))

main(sys.argv);
