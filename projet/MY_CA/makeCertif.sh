#!/bin/bash

export NB_BYTES=2048

# Nétoyage de toutes les anciennes clé
rm -rf *.key *.csr *.crt serverAuth stamp newcerts newCert

mkdir -p serverAuth/newcerts
mkdir -p stamp/newcerts
mkdir newCert
# Création des fichiers de base:
echo '1000' > serverAuth/serial.txt
echo '1000' > stamp/serial.txt
touch serverAuth/index.txt
touch stamp/index.txt
echo 'unique_subject = yes' > serverAuth/index.txt.attr
echo 'unique_subject = yes' > stamp/index.txt.attr

# Création du certificat racine
openssl genrsa -des3 -out ca_private.key $NB_BYTES
# Auto signature
openssl req -new -x509 -days 7300 -config root-ca.cnf -key ca_private.key -out ca_certif.crt

# Création de l'utilisateur SSL
openssl genrsa -out newCert/SSL-user.key $NB_BYTES
openssl req -new -config SSL-user.cnf -key newCert/SSL-user.key -out newCert/SSL-user.csr
openssl ca -config ca-sign.serverAuth.cnf -out newCert/SSL-user.crt -batch -infiles newCert/SSL-user.csr
# Generate the PEM file by just appending the key and certificate files:
cat newCert/SSL-user.key newCert/SSL-user.crt > newCert/SSL-user.pem

# Création de l'utilisateur Stamp
openssl genrsa -out newCert/Stamp.key $NB_BYTES
openssl req -new -config Stamp.cnf -key newCert/Stamp.key -out newCert/Stamp.csr
openssl ca -config ca-sign.Stamp.cnf -out newCert/Stamp.crt -batch -infiles newCert/Stamp.csr
openssl rsa -in newCert/Stamp.key -pubout -out newCert/Stamp.pub
# Generate the PEM file by just appending the key and certificate files:
cat newCert/Stamp.key newCert/Stamp.crt > newCert/Stamp.pem


# Generate a public/private key pair:
#openssl genrsa -out $FILENAME.key 1024
# Generate a self signed certificate:
#openssl req -new -key $FILENAME.key -x509 -days 3653 -out $FILENAME.crt
# Generate the PEM file by just appending the key and certificate files:
#cat $FILENAME.key $FILENAME.crt >$FILENAME.pem
