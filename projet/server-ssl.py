#!/usr/bin/python
# coding= utf8
import os, sys;
from BaseHTTPServer import HTTPServer;
from SimpleHTTPServer import SimpleHTTPRequestHandler;
from CGIHTTPServer import CGIHTTPRequestHandler;
import CGIHTTPServer;
import cgi, ssl;

__HTTPD_DIR__ = 'www';
__LISTEN__ = ('', 8888);# IP, PORT
__SSL_CRT__ = os.path.abspath('./MY_CA/newCert/SSL-user.crt');
__SSL_KEY__ = os.path.abspath('./MY_CA/newCert/SSL-user.pem');




################################################################################
# @brief Main
# @param[in] argv	{List<Str>} Les arguments de la ligne de commande
# @return {int} 0 Si tout s'est bien passé
##
def main( argv ):
	os.chdir(__HTTPD_DIR__);

	if os.path.isfile(__SSL_CRT__):
		print '%-40s[%s]'%('Certificate','\033[1m\033[32mFOUND\033[0m');
	else:
		print '%-40s[%s]'%('Certificate','\033[1m\033[31mNOT FOUND\033[0m');
		print 'File not found < '+__SSL_CRT__+' >';
		return 1;
	if os.path.isfile(__SSL_KEY__):
		print '%-40s[%s]'%('Private Key','\033[1m\033[32mFOUND\033[0m');
	else:
		print '%-40s[%s]'%('Private Key','\033[1m\033[31mNOT FOUND\033[0m');
		print 'File not found < '+__SSL_KEY__+' >';

	handler = CGIHTTPRequestHandler;
	#handler.cgi_directories = ['./cgi-bin/','./'];
	#handler.server_version = 'TimeStampProject/0.1';
	#handler.sys_version = 'GodSystem/42.0';
	#handler.protocol_version = 'HTTP/1.1';

	print 'Starting Server...';
	httpd = CGIHTTPServer.BaseHTTPServer.HTTPServer(__LISTEN__, handler);
	print 'Starting SSL...';
	httpd.socket = ssl.wrap_socket(httpd.socket, certfile=__SSL_CRT__, keyfile=__SSL_KEY__, server_side=True);
	print 'Server running';
	httpd.serve_forever();
	return 0;

if __name__ == '__main__':
	exit(main(sys.argv));
