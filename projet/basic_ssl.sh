#!/bin/bash
export PrKey='./MY_CA/newCert/SSL-user.key'
export CERT='./MY_CA/newCert/SSL-user.crt'
export PuKey='./MY_CA/newCert/SSL-user.csr'
export PEM='./MY_CA/newCert/SSL-user.pem'
socat openssl-listen:4443,reuseaddr,cert=$PEM,cafile=$CERT,fork,verify=0 tcp:localhost:8000
