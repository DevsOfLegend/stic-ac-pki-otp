#!/usr/bin/python2
# coding= utf8
import os, sys;
import commands, cgi;
import logging;
import subprocess;
import hashlib;
from base64 import b64encode, b64decode;
from json import dumps as toJSON, loads as fromJSON;
from time import time;
from urllib import quote;
from tempfile import mkstemp;
from OTP import GoogleAuthenticatorCode,__DEFAULT_PASSWORD__;

__DATABASE__ = {
	'god': __DEFAULT_PASSWORD__#!< Mot de passe en claire :D
};

# Même en étant dans cgi-bin, on est en faite dans www
__PRIVATE_KEY__ = '.././MY_CA/newCert/Stamp.key';
__CERT__ = '.././MY_CA/newCert/Stamp.crt';
__PUBLIC_KEY__ = '.././MY_CA/newCert/Stamp.pub';


################################################################################
# @brief Main
# @param[in] argv	{List<Str>} Les arguments de la ligne de commande
# @return {int} 0 Si tout s'est bien passé
##
def main( argv ):
	form = cgi.FieldStorage();

	# Permet de blocker la redirection
	#print 'Content-Type: text/txt';
	#print;

	try:
		if not os.path.isfile(__PRIVATE_KEY__):
			raise Exception('Clé privé introuvable ! <'+os.path.abspath(__PRIVATE_KEY__)+'>');

		if not os.path.isfile(__PUBLIC_KEY__):
			raise Exception('Clé public introuvable ! <'+os.path.abspath(__PUBLIC_KEY__)+'>');


		if not form.has_key('login') or not userExist(form['login'].value.strip(' \t\r\n')):
			raise Excp(('login','Le champs %name% a un Login incorrect'));

		otp = GoogleAuthenticatorCode(__DATABASE__[form['login'].value]);

		if not form.has_key('otp_pass') or not otp == form['otp_pass'].value:
			raise Excp(('otp_pass','Bad OTP password'));

		if not form.has_key('file') or not form['file'].value:
			raise Excp(('file','No file uploaded'));

		# Création d'un fichier temporaire unique
		trash, doc2cert = mkstemp(suffix='.upl', prefix=otp);

		with open(doc2cert, 'wb') as fp:
			fp.write(form['file'].value);

		cert = genStamp2(doc2cert, form['login'].value);
		if cert == None:
			raise Excp(('main','Site indisponible pour le moment.'));

	except Excp as m:
		raison = quote(m.raison[0],'')+'='+quote(m.raison[1],'');
		print 'Location: ../#'+raison;
		print 'Content-Type: text/html';
		print ;
		print '<script type="text/javascript">window.location.href="../#'+raison+'";</script>';
		return 0;

	except Exception as m:
		raison = 'main='+quote('Site indisponible pour le moment','');
		print 'Location: ../#'+raison;
		print 'Content-Type: text/html';
		print ;
		print '<script type="text/javascript">window.location.href="../#'+raison+'";</script>';
		logging.error(m);
		return 0;

	filename = form['login'].value+'.'+otp+'.hscert';
	print 'Content-Type: application/force-download; name="'+filename+'"';
	print 'Content-Transfer-Encoding: binary';
	print 'Content-Length: '+str(len(cert));
	print 'Content-Disposition: attachment; filename="'+filename+'"';
	print 'Expires: 0';
	print 'Cache-Control: no-cache, must-revalidate';
	print 'Pragma: no-cache';
	print ;
	print cert;

	return 0;


################################################################################
# @brief Permet de déterminer si un utilisateur donné est en BD
# @param[in] user	{Str} L'utilisateur a tester
# @return {Bool} TRUE si l'utilisateur est en DB. FALSE sinon
##
def userExist( user ):
	return user in __DATABASE__.keys();


################################################################################
# @brief Permet de déterminer si un password OTP est correct.
# @param[in] user	{Str} L'utilisateur a tester
# @param[in] otp	{Str} Le password OTP
# @return {Bool} TRUE si l'OTP est correct. FALSE sinon
##
def isGoodOTP( user, otp ):
	return GoogleAuthenticatorCode(__DATABASE__[user]) == otp;


################################################################################
# @brief Permet de générer un certificat de rendu pour une date donnée
# @param[in] fileToHorodate		{Str} Le fichier a horodater
# @param[in] username			{Str} Le nom d'utilisateur
# @return {Str} Format du cert: Base64(JSON({'date':...,'sign':...,'username':...,'sha1':...}))
#
# @note Version sans <openssl ts>
##
def genStamp2( fileToHorodate, username ):
	# Création d'un fichier temporaire unique
	trash, tmp_query = mkstemp();

	sha1 = genSHA1(fileToHorodate);
	mtime = time();

	# On ajout le nom de l'utilisateur comme preuve
	reSha1 = sha1Str(sha1+username);

	# On créer un fichier qui va contenir le reSha1 + la date
	with open(tmp_query+'.sha1','w') as fp:
		fp.write(reSha1+'\n'+str(mtime));

	# On signe ce fichier
	logging.info('~ Generate Cert');
	p = subprocess.Popen(['openssl', 'rsautl', '-sign', '-inkey', __PRIVATE_KEY__, '-in', tmp_query+'.sha1', '-out', tmp_query+'.sig'], shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
	if int(p.wait()) != 0:
		logging.error('Openssl(1) error:');
		logging.error(p.stdout.read());
		return None;

	# On récupère la signature
	sign = None;
	with open(tmp_query+'.sig','rb') as fp:
		sign = fp.read();

	return b64encode(toJSON({
		'date': mtime,
		'sha1': sha1,
		'sign': b64encode(sign),
		'username': username
	}));


################################################################################
# @brief Permet de vérifier l'horodatage d'un fichier
# @param[in] fileToHorodate		{Str} Le fichier a tester
# @param[in] cert				{Str} Certificat Horostamp. Format du cert: Base64(JSON({'date':...,'sign':...,'username':...,'sha1':...}))
# @return {Bool} TRUE si tout est OK. FALSE sinon
#
# @note Version sans <openssl ts>
##
def checkFile2( fileToHorodate, cert ):
	# Création d'un fichier temporaire unique
	trash, tmp_query = mkstemp();

	# On lit le fichier si c'est un fichier
	if os.path.isfile(cert):
		with open(cert, 'r') as fp:
			cert = fp.read();

	# On décode les données
	cert = fromJSON(b64decode(cert));

	# On vérifit si les SHA1 sont égaux
	if genSHA1(fileToHorodate) != cert['sha1']:
		return False;

	# On extrait la signature
	with open(tmp_query+'.sig', 'wb') as fp:
		fp.write(b64decode(cert['sign']));

	# On recréer une copie des données
	reSha1 = sha1Str(cert['sha1']+cert['username']);
	goodCopy = reSha1+'\n'+str(cert['date']);

	# On vérifit la signature
	logging.info('~ Check Cert');
	p = subprocess.Popen(['openssl', 'rsautl', '-verify', '-pubin', '-inkey', __PUBLIC_KEY__, '-in', tmp_query+'.sig', '-out', tmp_query+'.sha1'], shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
	if int(p.wait()) != 0:
		logging.error('Openssl(2) error:');
		logging.error(p.stdout.read());
		return None;

	# La signature doit être egale à goodCopy
	with open(tmp_query+'.sha1', 'r') as fp:
		return fp.read() == goodCopy;


################################################################################
# @brief Permet de générer un certificat de rendu pour une date donnée
# @param[in] fileToHorodate		{Str} Le fichier a horodater
# @param[in] username			{Str} Le nom d'utilisateur
# @return {Str} Format du cert: Base64(JSON({'date':...,'sign':...,'username':...,'sha1':...}))
#
# Les commandes suivantes sont sencer fonctionner:
#openssl ts -query -digest b7e5d3f93198b38379852f2c04e78d73abdd0f4b -no_nonce -out design1.tsq
#openssl ts -query -in design1.tsq -text
## La commmande qui suit NE MARCHE PAS. Ce qui rend impossible l'horodatage classique
#openssl ts -reply -queryfile design1.tsq -inkey $PrKey -signer $CERT -out design1.tsr
#openssl ts -verify -queryfile 1.tsq -in 1.tsr -CAfile $CERT.pem
##
def genStamp( fileToHorodate, username ):
	# Création d'un fichier temporaire unique
	trash, tmp_query = mkstemp(suffix='.sha1');

	# On ajout le nom de l'utilisateur comme preuve
	#with open(fileToHorodate, 'a') as fp:
	#	fp.write(username);

	logging.info('~ Generate SHA1');
	p = subprocess.Popen(['openssl', 'ts', '-query', '-data', fileToHorodate, '-sha1', '-out', tmp_query], shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
	if int(p.wait()) != 0:
		logging.error('Openssl(1) error:');
		logging.error(p.stdout.read());
		return None;

	mtime = time();
	sha1 = genSHA1(fileToHorodate);

	logging.info('~ Generate Cert');
	p = subprocess.Popen(['openssl', 'rsautl', '-sign', '-inkey', __PRIVATE_KEY__, '-in', tmp_query, '-out', tmp_query+'_f'], shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
	if int(p.wait()) != 0:
		logging.error('Openssl(2) error:');
		logging.error(p.stdout.read());
		return None;

	sign = None;
	with open(tmp_query+'_f', 'rb') as fp:
		sign = fp.read();

	return b64encode(toJSON({
		'date': mtime,
		'sha1': sha1,
		'sign': b64encode(sign),
		'username': username
	}));


################################################################################
# @brief Permet de vérifier l'horodatage d'un fichier
# @param[in] fileToHorodate		{Str} Le fichier a tester
# @param[in] cert				{Str} Certificat Horostamp. Format du cert: Base64(JSON({'date':...,'sign':...,'username':...,'sha1':...}))
# @return {Bool} TRUE si tout est OK. FALSE sinon
#
# @note DEVRAIT FONCTIONNER, MAIS SANS <openssl ts -reply...> IMPOSSIBLE DE TESTER
##
def checkFile( fileToHorodate, cert ):
	# Création d'un fichier temporaire unique
	trash, tmp_query = mkstemp(suffix='.sha1');

	if os.path.isfile(cert):
		with open(cert, 'r') as fp:
			cert = fromJSON(b64decode(fp.read()));
	else:
		cert = fromJSON(b64decode(cert));

	if genSHA1(fileToHorodate) != cert['sha1']:
		print 'Bad SHA1';
		return False;

	with open(tmp_query+'.sign', 'wb') as fp:
		fp.write(b64decode(cert['sign']));

	p = subprocess.Popen(['openssl', 'ts', '-verify', '-data', fileToHorodate, '-in', tmp_query+'.sign', '-CAfile', __PUBLIC_KEY__], shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
	if int(p.wait()) != 0:
		logging.error('Openssl(4) error:')
		logging.error(p.stdout.read().strip('\r\n\t '));
		return False;

	out = p.stdout.read().strip(' \t\r\n');
	logging.info(out);

	return out.endswith('Verification: OK');


################################################################################
# @brief Permet de gérer des exceptions plus simplement
##
class Excp( Exception ):
    def __init__(self,raison):
        self.raison = raison;

    def __str__(self):
        return self.raison;


################################################################################
# @brief Permet de hasher un fichier en SHA1
# @param[in] filename	{Str} Le nom du fichier a hasher
# @return {Str} Le fichier hasher en SHA1 version HEXA
##
def genSHA1( filename ):
	sha = hashlib.sha1();
	with open(filename, 'rb') as fp:
		sha.update(fp.read());
	return sha.hexdigest();


################################################################################
# @brief Permet de hasher un texte en SHA1
# @param[in] sh		{Str} Le texte a hasher
# @return {Str} Le texte hasher en SHA1 version HEXA
##
def sha1Str( sh ):
	sha = hashlib.sha1();
	sha.update(sh);
	return sha.hexdigest();


################################################################################
# @brief Permet décoder un certificat
# @param[in] cert		{Str} Le certificat a décoder
# @return {Dict} Le certificat décodé
##
def decodeCert( cert ):
	return fromJSON(b64decode(cert));


# Système de log
def log():
	formatter = logging.Formatter( '[%(levelname)-8s][%(asctime)s][%(filename)s:%(lineno)3d] %(funcName)s() :: %(message)s\n', datefmt='%Y/%m/%d %H:%M' );

	ch = logging.StreamHandler();
	ch.setLevel( logging.DEBUG );
	ch.setFormatter( formatter );
	logging.getLogger().addHandler( ch );

	fh = logging.FileHandler( 'Error.log', 'w' );
	fh.setLevel( logging.DEBUG );
	fh.setFormatter( formatter );
	logging.getLogger().addHandler( fh );
log();


if __name__ == '__main__':
	exit(main(sys.argv));
