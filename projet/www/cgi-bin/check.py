#!/usr/bin/python
# coding= utf8
import os, sys;
import commands, cgi;
import logging;
from urllib import quote;
from tempfile import mkstemp;
from cert import checkFile2, decodeCert, Excp, __PRIVATE_KEY__, __CERT__, __PUBLIC_KEY__;
import time;


################################################################################
# @brief Main
# @param[in] argv	{List<Str>} Les arguments de la ligne de commande
# @return {int} 0 Si tout s'est bien passé
##
def main( argv ):
	form = cgi.FieldStorage();

	# Permet de blocker la redirection
	#print 'Content-Type: text/txt';
	#print;

	try:
		if not os.path.isfile(__PRIVATE_KEY__):
			raise Exception('Clé privé introuvable ! <'+os.path.abspath(__PRIVATE_KEY__)+'>');

		if not os.path.isfile(__PUBLIC_KEY__):
			raise Exception('Clé public introuvable ! <'+os.path.abspath(__PUBLIC_KEY__)+'>');


		if (not form.has_key('file_doc') or not form['file_doc'].value) and (not form.has_key('file_doc_str') or not form['file_doc_str'].value.strip(' \t\r\n')):
			raise Excp(('file_doc','No file uploaded'));

		if (not form.has_key('file_proof') or not form['file_proof'].value) and (not form.has_key('file_proof_str') or not form['file_proof_str'].value.strip(' \t\r\n')):
			raise Excp(('file_proof','No file uploaded'));

		# Création d'un fichier temporaire unique
		trash, doc2cert = mkstemp(suffix='.upl');

		if form.has_key('file_doc') and form['file_doc'].value:
			with open(doc2cert, 'wb') as fp:
				fp.write(form['file_doc'].value);

		if form.has_key('file_doc_str') and form['file_doc_str'].value:
			with open(doc2cert, 'wb') as fp:
				fp.write(form['file_doc_str'].value);

		cert = None;
		if form.has_key('file_proof') and form['file_proof'].value:
			cert = form['file_proof'].value;

		if form.has_key('file_proof_str') and form['file_proof_str'].value:
			cert = form['file_proof_str'].value;

		chk = checkFile2(doc2cert, cert);
		cert = decodeCert(cert);

		if chk == True:
			cert['date'] = time.localtime(cert['date']);
			cert['date'] = time.strftime('%d-%m-%Y %H:%M:%S', cert['date']);
			raise Excp(('valid',cert['date']));
		elif chk == False:
			raise Excp(('main','Le document ayant pour SHA1: '+cert['sha1']+' ne poss&egrave;de pas de signature valide.'));
		else:
			# Erreur avec open SSL
			raise Exception('OpenSSL Error');

	except Excp as m:
		logging.info('pass');
		raison = quote(m.raison[0],'')+'='+quote(m.raison[1],'');
		print 'Location: ../#'+raison;
		print 'Content-Type: text/html';
		print ;
		print '<script type="text/javascript">window.location.href="../#'+raison+'";</script>';
		return 0;

	except Exception as m:
		raison = 'main='+quote('Site indisponible pour le moment','');
		print 'Location: ../#'+raison;
		print 'Content-Type: text/html';
		print ;
		print '<script type="text/javascript">window.location.href="../#'+raison+'";</script>';
		logging.error(m);
		return 0;

	return 0;




if __name__ == '__main__':
	exit(main(sys.argv));
