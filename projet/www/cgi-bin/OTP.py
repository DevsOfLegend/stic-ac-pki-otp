#!/usr/bin/python2
# coding= utf8
import os, sys;
from base64 import b32decode;
from time import time;
from hashlib import sha1 as hashlib_sha1;
from struct import pack, unpack;
from hmac import new as hmac_dgst;
from string import center, ljust;

__DEFAULT_PASSWORD__ = 'fnqurbkuyismvvqo';


"""
def GoogleAuthenticatorCode( secret ):
	key := base32decode(secret)
	message := current Unix time ÷ 30
	hash := HMAC-SHA1(key, message)
	offset := last nibble of hash
	truncatedHash := hash[offset..offset+3] //4 bytes starting at the offset
	Set the first bit of truncatedHash to zero //remove the most significant bit
	code := truncatedHash mod 1000000
	pad code with 0 until length of code is 6
	return code
"""
################################################################################
# @brief Permet de générer un OTP selon l'algo de Google.
# @param[in]
# @param[in,opt]
# @param[in,opt]
# @return
#
# @note Idée d'optimisation reprise de: https://github.com/tadeck/onetimepass/blob/master/onetimepass/__init__.py#L69
##
def GoogleAuthenticatorCode( secret, timeout=30, forceDate=None ):
	otp_time = None;
	if forceDate == None:
		otp_time = int(time()) / timeout;
	else:
		otp_time = int(forceDate) / timeout;

	try:
		key = b32decode(secret, casefold=True);# Attention si casefold=False, alors secret ne peut pas être un string !!!
	except TypeError as m:
		raise TypeError('Incorrect secret: '+str(m));

	# Convertion en "unsigned long long" (> big-endian)
	message = pack('>Q', otp_time);

	#>> hash := HMAC-SHA1(key, message)
	hmac_dst = hmac_dgst(key, message, hashlib_sha1).digest();

	# Rappel: org('a') == 65
	#>> offset := last nibble of hash
	offset = ord(hmac_dst[19]) & 15;

	# >I == Convertion en "unsigned int" (> = big-endian)
	#>> truncatedHash := hash[offset..offset+3] //4 bytes starting at the offset
	#>> Set the first bit of truncatedHash to zero //remove the most significant bit
	token_base = unpack('>I', hmac_dst[offset:offset + 4])[0] & 0x7fffffff;# Opti de https://github.com/tadeck/onetimepass/blob/master/onetimepass/__init__.py#L69

	#>> code := truncatedHash mod 1000000
	token = token_base % 1000000;
	#>> pad code with 0 until length of code is 6
	return '{:06d}'.format(token);


################################################################################
# @brief Permet de lancer des tests sur GoogleAuthenticatorCode
# @return[NONE]
##
def tests():
	secret = 'fnqurbkuyismvvqo';
	otp_test = {
		# Format: date:otp
		1365072961:'581561',
		1365073017:'626576',
	};
	timeout = 30;

	for date in otp_test:
		print 'OTP '+otp_test[date]+':',GoogleAuthenticatorCode(secret, timeout, date)==otp_test[date];


################################################################################
# @brief Main
# @param[in] argv	{List<Str>} Les arguments de la ligne de commande
# @return {int} 0 Si tout s'est bien passé
##
def main( argv ):
	if len(argv) < 1 or '-h' in argv or '--help' in argv:
		screen_width, screen_height = getTerminalSize();
		print center('', screen_width, '=');
		print '=\033[1m\033[34m'+center('OneTimePassword', screen_width-2)+'\033[0m=';
		print ljust('=  ./'+os.path.basename(__file__)+' [-h|--help] [password] [--tests]',screen_width-1)+'=';
		print ljust('=    password:   The password to use. If no password, default: '+__DEFAULT_PASSWORD__, screen_width-1)+'=';
		print ljust('=    -h|--help:  Print this help screen :D', screen_width-1)+'=';
		print ljust('=    --tests:     Start tests for Google OTP', screen_width-1)+'=';
		print ljust('=', screen_width-1)+'=';
		print center('', screen_width, '=');
		return 0;

	if '--tests' in argv:
		tests();
		return 0;

	if len(argv) == 2:
		print GoogleAuthenticatorCode(argv[1]);

	elif len(argv) == 1:
		print __DEFAULT_PASSWORD__+': '+GoogleAuthenticatorCode(__DEFAULT_PASSWORD__);

	else:
		print 'Bad argument ! See --help';

	return 0;


################################################################################
# @brief Permet d'obtenir les dimensions du terminal
# @return tuple: (width, height)
#
# @note Code repris de: http://www.aliendev.com/programming/snippet-friday-programming/quick-snippet-friday-center-text-in-a-console-python-program
##
def getTerminalSize():
    def ioctl_GWINSZ(fd):
        try:
            import fcntl, termios, struct, os;
            cr = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ,
        '1234'))
        except:
            return None
        return cr
    cr = ioctl_GWINSZ(0) or ioctl_GWINSZ(1) or ioctl_GWINSZ(2)
    if not cr:
        try:
            fd = os.open(os.ctermid(), os.O_RDONLY)
            cr = ioctl_GWINSZ(fd)
            os.close(fd)
        except:
            pass;
    if not cr:
        try:
            cr = (env['LINES'], env['COLUMNS'])
        except:
            cr = (25, 80)
    return int(cr[1]), int(cr[0])



if __name__ == '__main__':
	exit(main(sys.argv));
